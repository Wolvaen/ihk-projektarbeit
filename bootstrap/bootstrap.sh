#!/usr/bin/env bash

# Installing dependencies
apt-get update -y
apt-get install -y python-software-properties
apt-add-repository -y ppa:ondrej/apache2
add-apt-repository -y ppa:ondrej/php5-5.6
apt-get update -y
apt-get install -y vim git curl apache2
apt-get install -y php5 php5-mhash php5-mcrypt php5-curl php5-cli php5-mysql \
    php5-gd php5-intl php5-xsl php5-intl php5-dev php5-xdebug
debconf-set-selections <<< 'mysql-server mysql-server/root_password password password'
debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password password'
apt-get install -y mysql-server

# Symlink www folder
if ! [ -L /var/www ]; then
  rm -rf /var/www/html
  ln -fs /vagrant/web /var/www/html
fi

# Install composer
curl -Ss https://getcomposer.org/installer | php > /dev/null
sudo mv composer.phar /usr/bin/composer

# Copy ini-files
PHPINI_CLI_SRC=/vagrant/bootstrap/cli_php.ini
PHPINI_APACHE_SRC=/vagrant/bootstrap/apache_php.ini

PHPINI_CLI_TARGET=/etc/php5/cli/php.ini
PHPINI_APACHE_TARGET=/etc/php5/apache2/php.ini

cat $PHPINI_CLI_SRC > $PHPINI_CLI_TARGET
cat $PHPINI_APACHE_SRC > $PHPINI_APACHE_TARGET

sudo service apache2 restart

# Install NodeJS and Bower
curl -sL https://deb.nodesource.com/setup | sudo bash -
sudo apt-get install -y nodejs
sudo npm -g install npm@latest
sudo npm install -g bower