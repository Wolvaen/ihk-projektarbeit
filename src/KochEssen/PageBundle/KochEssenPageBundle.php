<?php

/*
 * Author: André Leifeld <a.leifeld@koch-essen.de>
 * Company: Koch Essen Kommunikation + Design GmbH
 */

namespace KochEssen\PageBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class KochEssenPageBundle extends Bundle {

    public function getParent() {
        return 'SonataPageBundle';
    }

}