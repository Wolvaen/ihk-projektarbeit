<?php

/*
 * Author: André Leifeld <a.leifeld@koch-essen.de>
 * Company: Koch Essen Kommunikation + Design GmbH
 */

namespace KochEssen\PageBundle\Admin;

use KochEssen\UserBundle\Controller\HostController;
use Sonata\PageBundle\Admin\SiteAdmin as BaseAdmin;

class SiteAdmin extends BaseAdmin {

    protected $hc;

    /**
     * Setter for HostController
     * @param HostController $hc
     */
    public function setHostController(HostController $hc){
        $this->hc = $hc;
    }

    /**
     * Custom query to get only the assigned Sites for the current user
     *
     * @param string $context
     * @return \Sonata\AdminBundle\Datagrid\ProxyQueryInterface
     */
    public function createQuery($context = 'list') {
        $query = parent::createQuery($context);
        $query = $this->hc->createQuery($query);
        return $query;
    }

}
