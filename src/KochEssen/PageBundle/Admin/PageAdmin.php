<?php

/*
 * Author: André Leifeld <a.leifeld@koch-essen.de>
 * Company: Koch Essen Kommunikation + Design GmbH
 */

namespace KochEssen\PageBundle\Admin;

use KochEssen\UserBundle\Controller\HostController;
use Sonata\PageBundle\Admin\PageAdmin as BaseAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Application\Sonata\PageBundle\Entity\Site;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

/**
 * Admin definition for the Page class
 *
 * @author Thomas Rabaix <thomas.rabaix@sonata-project.org>
 */
class PageAdmin extends BaseAdmin
{
    protected $hc;

    public function setHostController(HostController $hc) {
        $this->hc = $hc;
    }

    /**
     * {@inheritdoc}
     *
     * Override configureDatagridFilters to hide Sites from ROLE_SITE_ADMIN
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {

        parent::configureDatagridFilters($datagridMapper);

        if($this->hc->isSiteadmin()) {

            $datagridMapper->remove('site')
                ->add('site', null, array(), EntityType::class, array(
                    'choices' => array( $this->hc->getUser()->getSite() )
                ));
        }
    }

    /**
     * Override configureDatagridFilters to hide Sites from ROLE_SITE_ADMIN
     */
    protected function configureFormFields(FormMapper $formMapper) {

        parent::configureFormFields($formMapper);

        if($this->hc->isSiteadmin()) {
            $formMapper
                ->remove('site')
                ->add('site', HiddenType::class, array(
                    'class' => Site::class,
                    'choices' => array($this->hc->getUser()->getSite())
                ));
        }
    }

    /**
     * Custom query to get only the assigned Sites for the current user
     *
     * @param string $context
     * @return \Sonata\AdminBundle\Datagrid\ProxyQueryInterface
     */
    public function createQuery($context = 'list') {
        $query = parent::createQuery($context);
        $query = $this->hc->createQuery($query);
        return $query;
    }
}
