<?php

/*
 * Author: André Leifeld <a.leifeld@koch-essen.de>
 * Company: Koch Essen Kommunikation + Design GmbH
 */

namespace KochEssen\PageBundle\Controller;

use KochEssen\UserBundle\Controller\HostController;
use Sonata\PageBundle\Controller\PageAdminController as BaseController;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class PageAdminController extends BaseController
{

    protected $hc;

    /**
     * Getter for HostController
     *
     * @return HostController
     */
    public function getHostController(){
        if (!$this->hc) {
            $this->hc = $this->get('ke.user.host.controller');
        }
        return $this->hc;
    }

    /**
     * Override treeAction to filter Sites for ROLE_SITE_ADMIN
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function treeAction()
    {
        if (false === $this->admin->isGranted('LIST')) {
            throw new AccessDeniedException();
        }

        $hc = $this->getHostController();
        $sites = $hc->getSites();
        $pageManager = $this->get('sonata.page.manager.page');


        $currentSite = null;
        $siteId = $this->getRequest()->get('site');

        foreach ($sites as $site) {
            if ($siteId && $site->getId() == $siteId) {
                $currentSite = $site;
            } elseif (!$siteId && $site->getIsDefault()) {
                $currentSite = $site;
            }
        }
        if (!$currentSite && count($sites) == 1) {
            $currentSite = $sites[0];
        }

        if ($currentSite) {
            $pages = $pageManager->loadPages($currentSite);
        } else {
            $pages = array();
        }

        $datagrid = $this->admin->getDatagrid();
        $formView = $datagrid->getForm()->createView();

        $this->get('twig')->getExtension('form')->renderer->setTheme($formView, $this->admin->getFilterTheme());

        return $this->render('SonataPageBundle:PageAdmin:tree.html.twig', array(
            'action'      => 'tree',
            'sites'       => $sites,
            'currentSite' => $currentSite,
            'pages'       => $pages,
            'form'        => $formView,
            'csrf_token'  => $this->getCsrfToken('sonata.batch'),
        ));
    }

    /**
     * Override createAction to filter Sites for ROLE_SITE_ADMIN
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */

    public function createAction()
    {
        if (false === $this->admin->isGranted('CREATE')) {
            throw new AccessDeniedException();
        }

        if ($this->getRequest()->getMethod() == 'GET' && !$this->getRequest()->get('siteId')) {
            $hc = $this->getHostController();
            $sites = $hc->getSites();

            if (count($sites) == 1) {
                return $this->redirect($this->admin->generateUrl('create', array(
                    'siteId' => $sites[0]->getId(),
                    'uniqid' => $this->admin->getUniqid()
                )));
            }

            try {
                $current = $this->get('sonata.page.site.selector')->retrieve();
            } catch (\RuntimeException $e) {
                $current = false;
            }

            return $this->render('SonataPageBundle:PageAdmin:select_site.html.twig', array(
                'sites'   => $sites,
                'current' => $current,
            ));
        }

        return parent::createAction();
    }

}