<?php

/*
 * Author: André Leifeld <a.leifeld@koch-essen.de>
 * Company: Koch Essen Kommunikation + Design GmbH
 */

namespace KochEssen\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class KochEssenUserBundle extends Bundle {

    public function getParent() {
        return 'SonataUserBundle';
    }

}