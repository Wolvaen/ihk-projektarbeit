<?php

/*
 * Author: André Leifeld <a.leifeld@koch-essen.de>
 * Company: Koch Essen Kommunikation + Design GmbH
 */

namespace KochEssen\UserBundle\Model;

use Application\Sonata\PageBundle\Entity\Site;
use Sonata\UserBundle\Model\UserInterface as BaseInterface;

interface UserInterface extends BaseInterface
{
    /**
     * Gets a site from the user.
     *
     * @return Site
     */
    public function getSite();

    /**
     * Adds a site to the user.
     *
     * @param Site $host
     *
     * @return self
     */
    public function setSite($host);
}
