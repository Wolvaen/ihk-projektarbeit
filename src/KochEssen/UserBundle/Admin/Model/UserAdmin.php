<?php

/*
 * Author: André Leifeld <a.leifeld@koch-essen.de>
 * Company: Koch Essen Kommunikation + Design GmbH
 */

namespace KochEssen\UserBundle\Admin\Model;

use Sonata\UserBundle\Admin\Model\UserAdmin as BaseAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class UserAdmin extends BaseAdmin
{

    /**
     * Override configureListFields to filter Sites for ROLE_SITE_ADMIN
     *
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->add('site');

        parent::configureListFields($listMapper);
    }

    /**
     * Override configureShowFields to filter Sites for ROLE_SITE_ADMIN
     *
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->with('Site')
                ->add('site')
            ->end();

        parent::configureShowFields($showMapper);
    }

    /**
     * Override configureFormFields to filter Sites for ROLE_SITE_ADMIN
     *
     * @param ShowMapper $showMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Site')
                ->add('site', null,array('required'=>true))
            ->end();

        parent::configureFormFields($formMapper);
    }

}
