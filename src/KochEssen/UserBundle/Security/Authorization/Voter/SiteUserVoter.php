<?php

/*
 * Author: André Leifeld <a.leifeld@koch-essen.de>
 * Company: Koch Essen Kommunikation + Design GmbH
 */

namespace KochEssen\UserBundle\Security\Authorization\Voter;

use FOS\UserBundle\Model\UserInterface;
use Sonata\UserBundle\Security\Authorization\Voter\UserAclVoter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class SiteUserVoter extends UserAclVoter
{
    protected $container;

    public function setContainer($service_container) {
        $this->container = $service_container;
    }

    public function supportsClass($class)
    {
        return is_subclass_of($class, 'FOS\UserBundle\Model\UserInterface');
    }

    public function supportsAttribute($attribute)
    {
        return true;
    }

    /**
     * Custom Voter to prevent Site-Admins from access foreign domains.
     *
     * @param TokenInterface $token
     * @param null|object $object
     * @param array $attributes
     * @return int
     */
    public function vote(TokenInterface $token, $object, array $attributes)
    {
        $hc = $this->container->get('ke.user.host.controller');

        if (!$hc->hasSiteAccess()) {
            return self::ACCESS_DENIED;
        }

        return parent::vote($token, $object, $attributes);
    }
}