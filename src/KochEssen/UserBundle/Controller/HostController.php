<?php

/*
 * Author: André Leifeld <a.leifeld@koch-essen.de>
 * Company: Koch Essen Kommunikation + Design GmbH
 */

namespace KochEssen\UserBundle\Controller;

use KochEssen\UserBundle\Controller\Model\HostControllerInterface;
use KochEssen\UserBundle\Entity\User;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class HostController extends Controller implements HostControllerInterface
{
    /**
     * Check if User has Role ROLE_SITE_ADMIN and not ROLE_SUPER_ADMIN
     *
     * @return bool
     */
    public function isSiteAdmin() {
        $user = $this->getUser();
        if (!is_null($user)) {
            return !$user->hasRole(User::ROLE_SUPER_ADMIN) && $user->hasRole(User::ROLE_SITE_ADMIN);
        }
        return false;
    }

    /**
     * Helper method to create filtered Site query
     *
     * @param ProxyQueryInterface $query
     * @param string $field
     * @return ProxyQueryInterface
     */
    public function createQuery(ProxyQueryInterface $query, $field='site') {
        if(!$this->isSiteadmin()) {
            $site = $this->getUser()->getSite();
            if (!is_null($site)) {
                $id = $site->getId();
                $query->andWhere(
                    $query->expr()->eq($query->getRootAliases()[0] . '.' . $field,':' . ':id')
                );
                $query->setParameter('id', $id);
            }
        }
        return $query;
    }

    /**
     * Get all accesable Sites for current User
     *
     * @return array
     */
    public function getSites() {
        if($this->isSiteAdmin()) {
            return array($this->getUser()->getSite());
        } else {
            return $this->get('sonata.page.manager.site')->findBy(array());
        }
    }

    /**
     * Get current Host from Request
     *
     * @return string
     */
    public function getHost() {
        return $this->getRequest()->getHost();
    }

    /**
     * Check if user is SiteAdmin and is assigned to current Host.
     * Returns true if User is no SiteAdmin to delegate the access-decision to the voter.
     *
     * @return bool
     */
    public function hasSiteAccess() {
        if($this->isSiteAdmin()) {
            $site = $this->getUser()->getSite();
            if (!is_null($site)) {
                return $site->getHost() === $this->getHost();
            }
            return false;
        }
        return true;
    }
}