<?php

namespace KochEssen\UserBundle\Controller\Model;

use KochEssen\UserBundle\Controller\HostController;

class HostControllerMock extends HostController
{

    protected $user;
    protected $host;
    protected $siteAdmin;

    public function isSiteAdmin() {
        return $this->siteAdmin;
    }

    public function setSiteAdmin($siteAdmin) {
        $this->siteAdmin = $siteAdmin;
    }

    public function setHost($host) {
        $this->host = $host;
    }

    public function getHost() {
        return $this->host;
    }

    public function setUser($user) {
        $this->user = $user;
    }

    public function getUser() {
        return $this->user;
    }

}