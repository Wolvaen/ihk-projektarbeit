<?php

namespace KochEssen\UserBundle\Controller\Model;

use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;

interface HostControllerInterface
{

    public function isSiteAdmin();

    public function createQuery(ProxyQueryInterface $query, $field='site');

    public function getSites();

    public function hasSiteAccess();
}