<?php

/*
 * Author: André Leifeld <a.leifeld@koch-essen.de>
 * Company: Koch Essen Kommunikation + Design GmbH
 */

namespace KochEssen\UserBundle\Tests\Security\Authorization\Voter;

use Application\Sonata\PageBundle\Entity\Site;
use KochEssen\UserBundle\Controller\Model\HostControllerMock;
use KochEssen\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class HostControllerTeset extends WebTestCase
{
    protected $client;
    protected $container;

    public function getClient() {
        return $this->client;
    }

    public function setUp()
    {
        $this->client = self::createClient();
        $this->container = self::$kernel->getContainer();
    }

    protected static function getPhpUnitXmlDir()
    {
        return __DIR__.'/../../../../../app';
    }

    public function testHostControllerSiteAccessGranted()
    {
        // Given
        $testHost = 'example.com';
        $user = new User();
        $site = new Site();
        $hc = new HostControllerMock();

        // Set mock values
        $site->setHost($testHost);
        $user->setSite($site);
        $hc->setUser($user);
        $hc->setHost($testHost);
        $hc->setSiteAdmin(true);

        // When
        $user->addRole(User::ROLE_SITE_ADMIN);
        $user->setSite($site);
        $site->setHost($testHost);

        // Then
        $this->assertEquals($hc->getHost(), $testHost, 'Should be equal');
        $this->assertEquals($user->getSite()->getHost(), $testHost, 'Should be equal');
        $this->assertTrue($hc->isSiteAdmin(), 'Should be true');
        $this->assertTrue($hc->hasSiteAccess(), 'Should be true');
    }

}