<?php

/*
 * Author: André Leifeld <a.leifeld@koch-essen.de>
 * Company: Koch Essen Kommunikation + Design GmbH
 */

namespace KochEssen\UserBundle\Tests\Entity;

use Application\Sonata\PageBundle\Entity\Site;
use KochEssen\UserBundle\Entity\User;
use DateTime;

class BaseUserTest extends \PHPUnit_Framework_TestCase
{
    public function testUserSiteExtensionSetters()
    {
        // Given
        $user = new User();
        $today = new DateTime();
        $site = new Site();

        // When
        $user->addRole(User::ROLE_SITE_ADMIN);
        $user->setSite($site);
        $user->setCreatedAt($today);
        $user->setUpdatedAt($today);
        $user->setCredentialsExpireAt($today);

        // Then
        $this->assertTrue(in_array(User::ROLE_SITE_ADMIN, $user->getRoles()), 'Should contain '.User::ROLE_SITE_ADMIN);
        $this->assertTrue($user->isSiteAdmin(), 'Should return true');
        $this->assertTrue($user->getSite() instanceof Site, 'Should return a Site object');
        $this->assertEquals($user->getSite(), $site, 'Should contain our site object');
        $this->assertTrue($user->getUpdatedAt() instanceof DateTime, 'Should return a DateTime object');
        $this->assertEquals($today->format('U'), $user->getUpdatedAt()->format('U') , 'Should contain today\'s date');
    }
}
