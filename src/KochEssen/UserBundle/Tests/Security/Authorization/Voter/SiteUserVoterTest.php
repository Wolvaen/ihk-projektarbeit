<?php

/*
 * Author: André Leifeld <a.leifeld@koch-essen.de>
 * Company: Koch Essen Kommunikation + Design GmbH
 */

namespace KochEssen\UserBundle\Tests\Security\Authorization\Voter;

use KochEssen\UserBundle\Security\Authorization\Voter\SiteUserVoter;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;

class SiteUserVoterTest extends WebTestCase
{
    protected $client;
    protected $container;

    public function getClient() {
        return $this->client;
    }

    public function setUp()
    {
        $this->client = self::createClient();
        $this->container = self::$kernel->getContainer();
    }

    protected static function getPhpUnitXmlDir()
    {
        return __DIR__.'/../../../../../../../app';
    }

    /**
     * Testing the voter decision on a user
     * On default it will check the permission for a valid SiteAdmin
     *
     * @param bool $siteAccess
     * @param string $msg
     */
    public function testVoterAccess($siteAccess = true, $msg = "Should grant from voting")
    {
        $hc = $this->getMock('KochEssen\UserBundle\Controller\Model\HostControllerInterface');
        $hc->expects($this->any())->method('hasSiteAccess')->will($this->returnValue($siteAccess));
        $this->container->set('ke.user.host.controller', $hc);

        $user = $this->getMock('KochEssen\UserBundle\Model\UserInterface');

        $loggedInUser = $this->getMock('KochEssen\UserBundle\Model\UserInterface');

        $token = $this->getMock('Symfony\Component\Security\Core\Authentication\Token\TokenInterface');
        $token->expects($this->any())->method('getUser')->will($this->returnValue($loggedInUser));

        $aclProvider = $this->getMock('Symfony\Component\Security\Acl\Model\AclProviderInterface');
        $oidRetrievalStrategy = $this->getMock('Symfony\Component\Security\Acl\Model\ObjectIdentityRetrievalStrategyInterface');
        $sidRetrievalStrategy = $this->getMock('Symfony\Component\Security\Acl\Model\SecurityIdentityRetrievalStrategyInterface');
        $permissionMap = $this->getMock('Symfony\Component\Security\Acl\Permission\PermissionMapInterface');

        $voter = new SiteUserVoter($aclProvider, $oidRetrievalStrategy, $sidRetrievalStrategy, $permissionMap);
        $voter->setContainer($this->container);

        $decision = $voter->vote($token, $user, array('EDIT'));

        $access = ($siteAccess) ? VoterInterface::ACCESS_ABSTAIN : VoterInterface::ACCESS_DENIED;

        $this->assertEquals($access, $decision, $msg);
    }

    /**
     * Testing the voter decisions on a user with invalid site-permissions
     */
    public function testVoterDenied() {
        $this->testVoterAccess(false, "Should deny from voting");
    }

}