# Sonata Project Multisite-Erweiterung

Eine Erweiterung der Multisite-Funktionalität des SonataPageBundles um den Zugang eines Administratoren auf seine Website-Instanz zu beschränken.

## Bundles
- KochEssenUserBundle (/src/KochEssen/UserBundle)
- KochEssenPageBundle (/src/KochEssen/PageBundle)

## Dependencies
- PHP ">=5.6"
- SonataUserBundle "~2.2"
- SonataPageBundle "~2.3"

## Installation

1. Quellcode via Composer installieren oder im Verzeichnis "src" ablegen.
2. Die Bundles in der AppKernel.php registrieren:

            new KochEssen\UserBundle\KochEssenUserBundle('FOSUserBundle'),
            new KochEssen\PageBundle\KochEssenPageBundle()
3. Die Konfigurationsdateien der Bundles in die globale app/config.yml importieren:

		imports:
			- { resource: '@KochEssenUserBundle/Resources/config/config.yml' }
			- { resource: '@KochEssenPageBundle/Resources/config/config.yml' }
4. Das Datenbankschema updaten:

		$: app/console doctrine:database:schema:update --force

## Wie können Administratorrechte nun angepasst werden?

1. Um einen Admin auf eine Website-Instanz zu beschränken, muss ihm die Benutzerrolle *ROLE_SITE_ADMIN* zugewiesen werden.
2. Die Website-Instanz, auf welche der Admin beschränkt werden soll, muss in das *Host*-Feld eingetragen werden.
3. Die vorkonfigurierten Benutzerrechte für die Rolle ROLESITE_ADMIN sind in folgender Konfigurationsdatei zu finden und können nach Bedarf angepasst werden: 

		KochEssen/UserBundle/Resources/config/security.yml

**INFO:** Die Benutzerrechte können weiterhin, wie gewohnt anhand der Sonata Admin Oberfläche eingestellt werden.

